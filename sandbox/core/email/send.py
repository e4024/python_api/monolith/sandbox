import smtplib
from collections.abc import Iterable
from email.message import EmailMessage

from sandbox.core.email.settings import settings


def create_email_message(
    to_emails: Iterable[str], subject: str, message: str
) -> EmailMessage:
    email_message = EmailMessage()
    email_message["From"] = settings.EMAIL_LOGIN
    email_message["To"] = ", ".join(to_emails)
    email_message["Subject"] = subject
    email_message.set_content(message)
    return email_message


def send_email(to_emails: Iterable[str], subject: str, message: str) -> None:
    email_message = create_email_message(to_emails, subject, message)
    with smtplib.SMTP_SSL("smtp.gmail.com", 465) as smtp:
        smtp.login(settings.EMAIL_LOGIN, settings.EMAIL_PASSWORD)
        smtp.send_message(email_message)
