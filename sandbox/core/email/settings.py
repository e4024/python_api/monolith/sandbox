from pydantic import BaseSettings


class Settings(BaseSettings):
    EMAIL_LOGIN: str
    EMAIL_PASSWORD: str


settings = Settings()
