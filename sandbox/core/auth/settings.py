from datetime import timedelta

from pydantic import BaseSettings


class Settings(BaseSettings):
    JWT_SIGNING_ALGORITHM: str
    JWT_LIFETIME_MINUTES: int


settings = Settings()

JWT_LIFETIME = timedelta(minutes=settings.JWT_LIFETIME_MINUTES)

with open("/opt/app/jwt/jwtRS256.key") as jwt_private_key_file:
    JWT_PRIVATE_KEY = jwt_private_key_file.read()

with open("/opt/app/jwt/jwtRS256.key.pub") as jwt_public_key_file:
    JWT_PUBLIC_KEY = jwt_public_key_file.read()
