from datetime import datetime

from jose import jwt

from sandbox.core.auth.settings import (
    JWT_LIFETIME,
    JWT_PRIVATE_KEY,
    JWT_PUBLIC_KEY,
    settings,
)


def create_jwt(user_json: str) -> str:
    jwt_deadline = datetime.utcnow() + JWT_LIFETIME
    jwt_unsigned = {"sub": user_json, "exp": jwt_deadline}
    return jwt.encode(
        jwt_unsigned, JWT_PRIVATE_KEY, algorithm=settings.JWT_SIGNING_ALGORITHM
    )


def get_user_json_from_jwt(jwt_signed: str) -> str:
    payload: dict = jwt.decode(
        jwt_signed,
        JWT_PUBLIC_KEY,
        algorithms=[settings.JWT_SIGNING_ALGORITHM],
    )
    return payload["sub"]
