from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from sandbox.core.db.models import User


def fill_db_with_fake_data(session: Session) -> None:
    try:
        with session.begin():
            session.add(
                User(  # type: ignore[call-arg]
                    username="nero", password="nero", email="nero@gmail.com"
                )
            )
    except IntegrityError:
        pass
