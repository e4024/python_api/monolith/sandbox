import contextlib
import cProfile
import io
import pstats


@contextlib.contextmanager
def profiled():
    """
    Example:

    with profiled():
        Session.query(FooClass).filter(FooClass.somevalue == 8).all()
    """

    pr = cProfile.Profile()
    pr.enable()
    yield
    pr.disable()
    s = io.StringIO()
    ps = pstats.Stats(pr, stream=s).sort_stats("cumulative")
    ps.print_stats()
    # uncomment this to see who's calling what
    # ps.print_callers()
    print(s.getvalue())  # noqa: T201
