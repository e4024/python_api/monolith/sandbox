from datetime import date

from sqlalchemy import Boolean, Column, Date, String, extract, select
from sqlalchemy.orm import Session, relationship
from sqlalchemy.sql import exists
from sqlalchemy_utils import EmailType, PasswordType

from sandbox.core.db.models.base import Base


class User(Base):
    __tablename__ = "user"

    username = Column(String, nullable=False, unique=True)
    password = Column(PasswordType(schemes=["bcrypt"]), nullable=False)
    email = Column(EmailType, nullable=False, unique=True)
    is_superuser = Column(Boolean, nullable=False, default=False)
    birthday = Column(Date)

    purchases = relationship("Purchase", back_populates="user")

    @classmethod
    def get_by_username(cls, session: Session, username: str) -> "User":
        return session.execute(
            select(cls).where(cls.username == username)
        ).scalar_one()

    @classmethod
    def get_emails_of_those_who_has_birthday(
        cls, session: Session
    ) -> set[str]:
        today = date.today()
        return set(
            session.scalars(
                select(cls.email).where(
                    extract("MONTH", cls.birthday) == today.month,
                    extract("DAY", cls.birthday) == today.day,
                )
            ).all()
        )

    @classmethod
    def exists(cls, session: Session, id_: int) -> bool:
        return session.scalar(select(exists().where(cls.id == id_)))
