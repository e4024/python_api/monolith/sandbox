from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm import sessionmaker

from sandbox.core.db.settings import Settings
from sandbox.settings import DEBUG


def get_postgres_url():
    return Settings().get_postgres_url()


def get_engine(sqlalchemy_url):
    return create_engine(sqlalchemy_url, echo=DEBUG, future=True)


def get_sessionmaker(sqlalchemy_engine: Engine) -> sessionmaker:
    return sessionmaker(
        bind=sqlalchemy_engine,
        future=True,
        autoflush=False,
        expire_on_commit=False,
    )


postgres_url = get_postgres_url()
engine = get_engine(postgres_url)
session_getter = get_sessionmaker(engine)
