from sqlalchemy.orm import Session

from sandbox.core.db.models import User
from sandbox.core.email.send import send_email


def happy_birthday(session: Session):
    text = "С днем рождения!"

    emails = User.get_emails_of_those_who_has_birthday(session)
    if len(emails) > 0:
        send_email(emails, "Поздравление", text)
