from celery import Celery
from celery.schedules import crontab

from sandbox.celery.settings import settings

tasks_module = "sandbox.celery.tasks"

app = Celery(
    "tasks",
    backend=settings.CELERY_BACKEND_URL,
    broker=settings.CELERY_BROKER_URL,
    include=[f"{tasks_module}.tasks", f"{tasks_module}.beat"],
)

app.conf.beat_schedule = {
    "happy_birthday": {
        "task": f"{tasks_module}.beat.happy_birthday",
        "schedule": crontab(minute=0, hour=0),
    },
}
