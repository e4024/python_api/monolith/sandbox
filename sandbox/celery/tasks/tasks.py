from sandbox.celery.celery import app
from sandbox.core.cpu_bound import get_sum
from sandbox.core.email.send import send_email

get_sum = app.task(get_sum)
send_email = app.task(send_email)
