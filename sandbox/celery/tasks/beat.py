from sandbox.celery.celery import app
from sandbox.core.db.connect import session_getter
from sandbox.core.reports import birthday


@app.task
def happy_birthday():
    with session_getter() as session:
        birthday.happy_birthday(session)
