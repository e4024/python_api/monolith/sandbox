from pydantic import BaseSettings


class Settings(BaseSettings):
    DEBUG: bool = False


settings = Settings()

DEBUG = settings.DEBUG
