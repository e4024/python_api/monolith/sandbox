from fastapi import APIRouter, Depends

from sandbox.api.constants import EndpointTag
from sandbox.api.dependencies.auth import get_user_by_credentials
from sandbox.api.handlers import auth as handlers
from sandbox.api.schemas.auth import JWT
from sandbox.api.schemas.user import User

router = APIRouter(prefix="/auth", tags=[EndpointTag.AUTH])


@router.post("/jwt", response_model=JWT, response_description="Созданный JWT")
async def create_jwt(user: User = Depends(get_user_by_credentials)) -> JWT:
    """Для входа в систему и получения JWT"""
    return handlers.create_jwt(user)
