from fastapi import APIRouter, Body, Depends, Query, status
from loguru import logger
from pydantic import EmailStr
from sqlalchemy.orm import Session

from sandbox.api.constants import EndpointTag
from sandbox.api.dependencies.common import (
    PaginationQueryParams,
    get_id_query_param,
    get_session,
)
from sandbox.api.dependencies.user import get_id_of_user_for_change
from sandbox.api.handlers import user as handlers
from sandbox.api.schemas.user import User, UserCreate, UserPage, UserUpdate

router = APIRouter(prefix="/users", tags=[EndpointTag.USER])


@router.get(
    "/page",
    response_model=UserPage,
    summary="Получить страницу с пользователями",
    response_description="Страница с пользователями",
)
async def get_user_page(
    session: Session = Depends(get_session),
    pagination_params: PaginationQueryParams = Depends(),
    id_: int | None = Depends(get_id_query_param),
    username: str
    | None = Query(
        None, description="Пользователя с каким юзернеймом выбрать?"
    ),
    email: EmailStr
    | None = Query(
        None,
        title="Електронная почта",
        description="Пользователя с каким email выбрать?",
    ),
) -> UserPage:
    """Для фильтрации списка пользователей, с пагинацией"""
    logger.info("Page!")
    return await handlers.get_user_page(
        session,
        pagination_params.offset,
        pagination_params.limit,
        id=id_,
        username=username,
        email=email,
    )


@router.post("/", response_model=User, status_code=status.HTTP_201_CREATED)
def create_user(
    session: Session = Depends(get_session), user: UserCreate = Body(...)
) -> User:
    """Для создания пользователя"""
    return handlers.create_user(session, user)


@router.patch(
    "/{id}",
    response_model=User,
    response_description="Обновленный пользователь",
)
def update_user(
    session: Session = Depends(get_session),
    id_of_user_for_update: int = Depends(get_id_of_user_for_change),
    user_fields_for_update: UserUpdate = Body(...),
) -> User:
    """Для обновления пользователя"""
    return handlers.update_user(
        session, id_of_user_for_update, user_fields_for_update
    )


@router.delete(
    "/{id}",
    status_code=status.HTTP_204_NO_CONTENT,
    response_description="Количество удаленных объектов",
)
def delete_user(
    session: Session = Depends(get_session),
    id_of_user_for_delete: int = Depends(get_id_of_user_for_change),
) -> None:
    handlers.delete_user(session, id_of_user_for_delete)
