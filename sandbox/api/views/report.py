from fastapi import APIRouter, Depends

from sandbox.api.dependencies.auth import get_current_active_user
from sandbox.api.handlers import report
from sandbox.api.schemas.user import User

router = APIRouter(prefix="/report")


@router.get("/")
def send_report(user: User = Depends(get_current_active_user)):
    report.send_report(user)
