from fastapi import APIRouter

from sandbox.api.handlers import cpu_bound as handlers

router = APIRouter(prefix="/cpu_bound")


@router.post("/sum")
async def get_sum(a: int, b: int):
    return handlers.get_sum(a, b)
