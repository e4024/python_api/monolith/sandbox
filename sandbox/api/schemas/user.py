from datetime import date

from pydantic import EmailStr, validator

from sandbox.api.schemas.base import Base, BaseOrm, BaseOrmPage, BaseOrmUpdate


def username_ascii(username: str):
    if username.isascii():
        return username
    else:
        raise ValueError("Username содержит не ascii символы")


class BaseUser(Base):
    username: str
    email: EmailStr
    birthday: date | None

    _username_ascii = validator("username", allow_reuse=True)(username_ascii)


class UserCreate(BaseUser):
    password: str


class UserUpdate(BaseOrmUpdate):
    username: str | None
    password: str | None
    email: EmailStr | None
    birthday: date | None

    @validator("username", "password", "email")
    def _value_is_not_none(cls, v, field):
        if v is None:
            raise ValueError(f"{field.name} is None")
        else:
            return v

    _username_ascii = validator("username", allow_reuse=True)(username_ascii)


class User(BaseUser, BaseOrm):
    is_superuser: bool


class UserPage(BaseOrmPage):
    items: list[User]
