from pydantic import Field

from sandbox.api.schemas.base import Base


class Credentials(Base):
    username: str = Field(...)
    password: str = Field(...)


class JWT(Base):
    access_token: str
    token_type: str
