from abc import ABC, abstractmethod
from datetime import datetime
from typing import TypeVar

from orjson import dumps, loads
from pydantic import BaseModel, Field

from sandbox.core.db.models.base import B, Page


# Взял из документации Pydantic
def _orjson_dumps(v, *, default):
    # orjson.dumps returns bytes,
    # to match standard json.dumps we need to decode
    return dumps(v, default=default).decode()


class Base(BaseModel):
    class Config:
        json_loads = loads
        json_dumps = _orjson_dumps


BO = TypeVar("BO", bound="BaseOrm")


class BaseOrm(Base):
    id: int = Field(
        ...,
        ge=1,
        title="Уникальный идентификатор",
        description="Для однозначного определения объекта",
    )
    created_at: datetime = Field(
        ..., title="Дата создания", description="Когда был создан объект"
    )
    updated_at: datetime | None = Field(
        ..., title="Дата обновления", description="Когда был обновлен объект"
    )
    is_active: bool = Field(
        ...,
        title="Активность",
        description=(
            "Переключатель активности объекта. Нужно ли его прятать или нет?"
        ),
    )

    class Config(Base.Config):
        orm_mode = True

    @classmethod
    def construct_from_orm(cls: type[BO], orm_obj: B) -> BO:
        """База данных это доверенный источник. Можно создавать pydantic
        объекты без проверки типов"""
        values = {
            field_name: getattr(orm_obj, field_name)
            for field_name in cls.__fields__.keys()
        }
        return cls.construct(**values)


class BaseOrmUpdate(Base):
    is_active: bool | None = Field(
        None,
        title="Активность",
        description=(
            "Переключатель активности объекта. Нужно ли его прятать или нет?"
        ),
        example=True,
    )


BOP = TypeVar("BOP", bound="BaseOrmPage")


class BaseOrmPage(Base, ABC):
    total: int = Field(
        ...,
        ge=0,
        title="Всего",
        description=(
            "Количество объектов в базе, подходящих под заданные условия"
        ),
    )
    offset: int = Field(
        ...,
        ge=0,
        title="Отступ",
        description=(
            "Количество объектов в начале списка которые были пропущены"
        ),
    )
    limit: int | None = Field(
        None,
        ge=0,
        title="Лимит",
        description="Количество объектов которое запросили из базы",
    )

    @property
    @abstractmethod
    def items(self) -> list[BO]: pass

    @classmethod
    def construct_from_page(cls: type[BOP], page: Page) -> BOP:
        """Создает объект из страницы орм объектов, без проверки типов"""
        schema = cls.__fields__["items"].type_
        items = [schema.construct_from_orm(item) for item in page.items]
        return cls.construct(
            total=page.total, offset=page.offset, limit=page.limit, items=items
        )
