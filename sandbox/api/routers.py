from fastapi import APIRouter

from sandbox.api.views.auth import router as auth_router
from sandbox.api.views.cpu_bound import router as cpu_bound_router
from sandbox.api.views.health import router as health_router
from sandbox.api.views.hostname import router as hostname_router
from sandbox.api.views.report import router as report_router
from sandbox.api.views.user import router as user_router

root_router = APIRouter(prefix="/api")

public_router = APIRouter(prefix="/public")
private_router = APIRouter(prefix="/private")

for router in (
    hostname_router,
    auth_router,
    user_router,
    cpu_bound_router,
    report_router,
):
    public_router.include_router(router)

private_router.include_router(health_router)

root_router.include_router(public_router)
root_router.include_router(private_router)
