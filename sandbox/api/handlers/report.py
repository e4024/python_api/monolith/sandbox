from sandbox.api.schemas.user import User
from sandbox.celery.tasks.tasks import send_email


def send_report(user: User):
    report = "Все хорошо"
    send_email.delay(  # type: ignore[attr-defined]
        [user.email], "Отчет", report
    )
