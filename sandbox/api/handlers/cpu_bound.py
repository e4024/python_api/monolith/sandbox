from sandbox.celery import tasks


def get_sum(a: int, b: int):
    tasks.get_sum.delay(a, b)  # type: ignore[attr-defined]
