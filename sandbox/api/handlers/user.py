from typing import Any

import anyio
from sqlalchemy.orm import Session

from sandbox.api.constants import DEFAULT_LIMIT, DEFAULT_OFFSET
from sandbox.api.schemas.user import User, UserCreate, UserPage, UserUpdate
from sandbox.core.db import models


async def get_user_page(
    session: Session,
    offset: int = DEFAULT_OFFSET,
    limit: int | None = DEFAULT_LIMIT,
    **kwargs: Any
) -> UserPage:
    filters = {key: val for key, val in kwargs.items() if val is not None}
    user_page = await anyio.to_thread.run_sync(
        models.User.get_page, session, filters, offset, limit
    )
    return UserPage.construct_from_page(user_page)


def create_user(session: Session, user: UserCreate) -> User:
    return User.construct_from_orm(models.User.create(session, user.dict()))


def update_user(
    session: Session, id_: int, user_fields_for_update: UserUpdate
) -> User:
    return User.construct_from_orm(
        models.User.update(
            session, id_, user_fields_for_update.dict(exclude_unset=True)
        )
    )


def delete_user(session: Session, id_: int) -> None:
    models.User.delete(session, id_)
