from sandbox.api.schemas.auth import JWT
from sandbox.api.schemas.user import User
from sandbox.core.auth import jwt


def create_jwt(user: User) -> JWT:
    """Создает jwt"""
    jwt_signed = jwt.create_jwt(user.json())
    return JWT(access_token=jwt_signed, token_type="bearer")
