from sys import stderr

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import ORJSONResponse
from loguru import logger

from sandbox.api.constants import APP_DESCRIPTION, OPENAPI_TAGS, ORIGINS
from sandbox.api.dev.middlewares import add_process_time_header
from sandbox.api.routers import root_router
from sandbox.core.db.connect import session_getter
from sandbox.core.db.dev.fixtures import fill_db_with_fake_data
from sandbox.settings import DEBUG

logger.add(
    stderr,
    diagnose=DEBUG,
    level="DEBUG" if DEBUG else "INFO",
    colorize=True,
    backtrace=True,
    # serialize=True,
)

app = FastAPI(
    debug=DEBUG,
    title="sandbox",
    description=APP_DESCRIPTION,
    openapi_tags=OPENAPI_TAGS,
    redoc_url="/docs2",
    default_response_class=ORJSONResponse,
)

app.include_router(root_router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

if DEBUG:
    app.middleware("http")(add_process_time_header)

    @app.on_event("startup")
    def set_fixtures() -> None:
        with session_getter() as session:
            fill_db_with_fake_data(session)
