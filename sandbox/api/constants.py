from enum import StrEnum, auto, unique

from sandbox.settings import settings

APP_DESCRIPTION = "Учебный пример разного функционала"


@unique
class EndpointTag(StrEnum):
    AUTH = auto()
    USER = auto()


OPENAPI_TAGS = [
    {
        "name": EndpointTag.AUTH.value,
        "description": "Ендпоинты для авторизации, аутентификации",
    },
    {
        "name": EndpointTag.USER.value,
        "description": "Ендпоинты для работы с пользователями",
    },
]

if settings.DEBUG:
    ORIGINS = ["*"]
else:
    ORIGINS = [
        "http://localhost",
        "http://localhost:80",
    ]

DEFAULT_OFFSET = 0
DEFAULT_LIMIT = None
