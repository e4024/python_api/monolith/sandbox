from fastapi import Depends, HTTPException, status
from sqlalchemy.orm import Session

from sandbox.api.dependencies.auth import get_current_active_user
from sandbox.api.dependencies.common import get_id_path_param, get_session
from sandbox.api.schemas.user import User
from sandbox.core.db import models


def get_user_id_and_check_existence(
    session: Session = Depends(get_session),
    id_: int = Depends(get_id_path_param),
) -> int:
    if not models.User.exists(session, id_):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Пользователь не найден",
        )
    return id_


async def get_id_of_user_for_change(
    current_user: User = Depends(get_current_active_user),
    id_of_user_for_change: int = Depends(get_user_id_and_check_existence),
) -> int:
    if current_user.is_superuser or current_user.id == id_of_user_for_change:
        return id_of_user_for_change
    else:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Нельзя редактировать чужого пользователя",
        )
