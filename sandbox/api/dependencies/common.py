from dataclasses import dataclass

from fastapi import Path, Query

from sandbox.api.constants import DEFAULT_LIMIT, DEFAULT_OFFSET
from sandbox.core.db.connect import session_getter


async def get_session():
    session = session_getter()
    try:
        yield session
    finally:
        await session.close()


@dataclass
class PaginationQueryParams:
    offset: int = Query(
        DEFAULT_OFFSET,
        ge=0,
        title="Отступ",
        description="Для пагинации. Сколько объектов пропустить с начала?",
    )
    limit: int | None = Query(
        DEFAULT_LIMIT,
        ge=0,
        title="Лимит",
        description="Для пагинации. Сколько объектов выбрать?",
    )


async def get_id_query_param(
    id_: int
    | None = Query(
        None,
        alias="id",
        ge=1,
        title="Уникальный идентификатор",
        description="Объект с каким id выбрать?",
    )
):
    return id_


async def get_id_path_param(id_: int = Path(..., alias="id", ge=1)):
    return id_
