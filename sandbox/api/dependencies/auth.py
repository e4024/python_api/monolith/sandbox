from fastapi import Body, Depends, HTTPException, status
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from jose import JWTError
from pydantic import ValidationError
from sqlalchemy.orm import Session

from sandbox.api.dependencies.common import get_session
from sandbox.api.schemas.auth import Credentials
from sandbox.api.schemas.user import User
from sandbox.core.auth.jwt import get_user_json_from_jwt
from sandbox.core.db import models


def get_user_by_credentials(
    db: Session = Depends(get_session), credentials: Credentials = Body(...)
) -> User:
    """Находит пользователя по credentials"""

    # TODO: UserOrm.get_by_username() может выдать ошибку. Отлови ее!
    user = models.User.get_by_username(db, credentials.username)
    if user.password != credentials.password:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return User.construct_from_orm(user)


http_bearer = HTTPBearer()


async def get_current_user(
    token: HTTPAuthorizationCredentials = Depends(http_bearer),
) -> User:
    try:
        user = User.parse_raw(get_user_json_from_jwt(token.credentials))
    except (JWTError, KeyError, ValidationError):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )

    return user


async def get_current_active_user(
    current_user: User = Depends(get_current_user),
) -> User:
    if not current_user.is_active:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Inactive user"
        )
    return current_user
