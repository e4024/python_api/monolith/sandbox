# sandbox

## Описание
Это мой личный, учебный проект, песочница, в которой я собираю хорошие практики для создания python web приложения.

## Пример
https://edu-sandbox.tk/docs

Пока что, запущенный проект доступен по этому адресу.

## Технологии
Для запуска нужно установить docker, docker-compose

Основные python библиотеки: fastapi, pydantic, sqlalchemy1.4, pytest, celery.

## Подготовка к запуску

### Переменные окружения
В корне проекта создайте файл .env аналогичный этому

```
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
POSTGRES_DB=postgres
POSTGRES_HOST=postgres
POSTGRES_PORT=5432

JWT_SIGNING_ALGORITHM=RS256
JWT_LIFETIME_MINUTES=2880

CELERY_BROKER_URL=redis://redis:6379/0
CELERY_BACKEND_URL=redis://redis:6379/0

EMAIL_LOGIN=example@gmail.com
EMAIL_PASSWORD=example
```

### Ключи шифрования для jwt
Выполните команды:

```
ssh-keygen -t rsa -b 4096 -m PEM -f jwtRS256.key
openssl rsa -in jwtRS256.key -pubout -outform PEM -out jwtRS256.key.pub
```

Полученные файлы вставьте в папку configs/jwt

### Конфиг для pgadmin4
Создайте файл configs/pgadmin4/servers.json аналогичный этому:

```
{
  "Servers": {
    "1": {
      "Name": "db",
      "Group": "Servers",
      "Port": 5432,
      "Username": "postgres",
      "Host": "postgres",
      "SSLMode": "prefer",
      "MaintenanceDB": "postgres"
    }
  }
}
```

Также пример можно найти здесь https://www.pgadmin.org/docs/pgadmin4/latest/import_export_servers.html#json-format

## Запуск
Для запуска используется docker-compose с ключем --profile. Он контролирует, что именно вы хотите сделать с помощью кода. Он может принимать значения:

1) prod - версия для продакшена
2) dev - версия для разработки
3) migrations - для работы с alembic и миграциями
4) tests - для запуска тестов
5) tests_dev - для разработки тестов, запуск с дебагером

Например, команда для запуска проекта для разработки локально:

`docker-compose --profile dev up --build`

Для работы с утилитой alembic и миграциями:

`docker-compose --profile migrations run migrations bash`

## Для разработки
Установите зависимости для разработки:

`poetry update --with dev`

Перед тем как делать коммиты запутите команду:

`pre-commit install`

При каждом коммите будут выполняться проверки качества кода. Если 1 из проверок не пройдет, коммит не будет создан.
