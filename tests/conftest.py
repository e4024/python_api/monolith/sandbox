from collections.abc import Generator
from uuid import uuid4

import pytest
from alembic.command import upgrade
from alembic.config import Config
from sqlalchemy.engine import URL, Engine, make_url
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy_utils import create_database, drop_database

from sandbox.core.db.connect import (
    get_engine,
    get_postgres_url,
    get_sessionmaker,
)
from sandbox.core.db.dev.fixtures import fill_db_with_fake_data


@pytest.fixture()
def temp_db_name_env(monkeypatch):
    """Мокает имя базы данных, что бы alembic создал правильную ссылку и
    выполнил миграции"""

    temp_db_name = f"{uuid4().hex}_pytest"
    monkeypatch.setenv("POSTGRES_DB", temp_db_name)


@pytest.fixture()
def temp_db_url(temp_db_name_env) -> Generator[URL, None, None]:
    temp_db_url: URL = make_url(get_postgres_url())
    create_database(temp_db_url)
    try:
        yield temp_db_url
    finally:
        drop_database(temp_db_url)


@pytest.fixture()
def alembic_engine(temp_db_url: URL) -> Generator[Engine, None, None]:
    """Нужно также для тестов из модуля pytest-alembic"""
    engine = get_engine(temp_db_url)
    try:
        yield engine
    finally:
        engine.dispose()


@pytest.fixture()
def session_getter(alembic_engine: Engine, temp_db_url: URL):
    config = Config("alembic.ini")
    upgrade(config, "head")

    session_getter = get_sessionmaker(alembic_engine)

    session = session_getter()
    with session:
        fill_db_with_fake_data(session)

    return session_getter


@pytest.fixture()
def session(session_getter: sessionmaker) -> Generator[Session, None, None]:
    session = session_getter()
    with session:
        yield session
