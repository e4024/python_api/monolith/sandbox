from pytest import mark
from sqlalchemy import select
from sqlalchemy.orm import Session

from sandbox.core.db.exceptions import ObjectNotFoundInDBError
from sandbox.core.db.models import User


def test_get_select_with_where() -> None:
    stmt = User._get_select_with_where(
        {"email": None, "username": "remi", "qwerty": 23}
    )
    assert (
        str(stmt.whereclause)
        == '"user".email IS NULL AND "user".username = :username_1'
    )


def test_get_objects_with_offset_limit(session: Session) -> None:
    stmt = select(User)
    expected = [session.get(User, 1)]
    result = User._get_objects_with_offset_limit(session, stmt, 0, None)
    assert result == expected

    result = User._get_objects_with_offset_limit(session, stmt, 50, 100)
    assert result == []


def test_get_page(session: Session) -> None:
    page = User.get_page(session, {"username": "nero"})
    assert (
        page.total == 1
        and page.offset == 0
        and page.limit is None
        and (item := page.items[0]).username == "nero"
        and item.password == "nero"
        and item.email == "nero@gmail.com"
    )


def test_create(session: Session) -> None:
    user_data = {
        "username": "migo",
        "password": "migo",
        "email": "migo@gmail.com",
    }
    user = User.create(session, user_data)
    user_from_db = session.scalar(
        select(User).where(
            User.username == user_data["username"],
            User.email == user_data["email"],
        )
    )

    assert (
        user is user_from_db
        and user_from_db.username == user_data["username"]
        and user_from_db.email == user_data["email"]
    )


def test_update(session: Session) -> None:
    user = session.get(User, 1)
    session.expunge(user)
    updated_user = User.update(session, 1, {"username": "kilo"})
    assert (
        updated_user is not user
        and updated_user.id == user.id
        and updated_user.username != user.username
    )


def test_delete(session: Session) -> None:
    assert User.delete(session, 1) is None


@mark.xfail(raises=ObjectNotFoundInDBError)
def test_delete_raise_exception(session: Session):
    User.delete(session, 1000)
    User.delete(session, -1000)


def test_user_exists(session: Session):
    assert User.exists(session, 1)


# def test_smthn(session: Session):
#     breakpoint()
